
/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/

#include <pybind11/pybind11.h>
#include <pybind11/embed.h>


#include <DecimaterPlugin.hh>

#include <OpenFlipper/BasePlugin/PythonFunctions.hh>
#include <OpenFlipper/PythonInterpreter/PythonTypeConversions.hh>

namespace py = pybind11;



PYBIND11_EMBEDDED_MODULE(Decimater, m) {

  QObject* pluginPointer = getPluginPointer("Decimater");

  if (!pluginPointer) {
     std::cerr << "Error Getting plugin pointer for Plugin-Decimater" << std::endl;
     return;
   }

  DecimaterPlugin* plugin = qobject_cast<DecimaterPlugin*>(pluginPointer);

  if (!plugin) {
    std::cerr << "Error converting plugin pointer for Plugin-Decimater" << std::endl;
    return;
  }

  // Export our core. Make sure that the c++ worlds core object is not deleted if
  // the python side gets deleted!!
  py::class_< DecimaterPlugin,std::unique_ptr<DecimaterPlugin, py::nodelete> > decimater(m, "Decimater");

  // On the c++ side we will just return the existing core instance
  // and prevent the system to recreate a new core as we need
  // to work on the existing one.
  decimater.def(py::init([plugin]() { return plugin; }));


  decimater.def("decimate",  static_cast<void (DecimaterPlugin::*) (int,QString)>(&DecimaterPlugin::decimate),
                                 QCoreApplication::translate("PythonDocDecimater","Decimate a mesh. You can use the following parameters:\n\n\
decimater_type [0 (Incremental), 1 (MC), 2 (Mixed)] \n\
random_samples [For MC/Mixed] \n\
incremental_percentage [For Mixed] \n\
decimation_order [0 (by distance), 1 (by normal deviation), 2 (by edge length)] \n\
distance \n\
edge_length \n\
normal_deviation \n\
roundness \n\
aspect_ratio,independent_sets \n\
vertices \n\
triangles \n\
selection_only\n\
\n\
\n\
Example call: \n\
decimater.decimate(5,\"random_samples=2;decimater_type=2;distance=0.0000001;decimation_order=0\") " ).toLatin1().data(),
                         py::arg(QCoreApplication::translate("PythonDocDecimater","ID of the object").toLatin1().data()),
                         py::arg(QCoreApplication::translate("PythonDocDecimater","List of Decimation parameters as a list separated by ';'").toLatin1().data()) );
}

